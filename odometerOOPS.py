class odometer :
    reading = int()
    def __init__(self) :
        digit = 1;
        n = 3;
        odometer_temp = "";
        for digit in range(1,n+1):
            odometer_temp += str(digit);
            digit += 1
        self.reading=int(odometer_temp)

    def isAscending(self):
        i = 1;
        cur_reading = str(self.reading);
        for i in range(1,len(cur_reading)):
            if cur_reading[i] <= cur_reading[i-1]:
                return False;
        return True;

    def nextReading(self):
        if (self.reading == 789):
            self.reading = 123
            return self.reading
        self.reading += 1
        while (not(self.isAscending())):
            self.reading += 1;
        return self.reading;

    def prevReading(self):
        if self.reading == 123:
            self.reading = 789
            return self.reading;
        self.reading -= 1;
        while (not(self.isAscending())):
            self.reading -= 1
        return self.reading;

    def next_n_readings(self,n):
        nextnreadings = [];
        for i in range(n):
            self.reading = nextReading();
            nextnreadings.append(self.reading);
        return nextnreadings;

    def prev_n_readings(self, n):
        prevnreadings = [];
        for i in range(n):
            self.reading = prevReading();
            prevnreadings.append(self.reading);
        return prevnreadings;

    def findDistance(self, reading1,reading2):
        count = 0;
        while(reading1 != reading2):
            reading1 = nextReading();
            count += 1
        return count;
